function init(){

}

$(document).ready(function (){

});

$(document).on("click", "#btnAkses", function () {
    if ($('#role_id').val() == 1) {
        $('#lblHeader').html("Akses Petugas");
        $('#btnAkses').html("Akses Pelapor");
        $('#role_id').val(2);
        $('#img-icon').attr('src', 'public/2.png');
    } else {
        $('#lblHeader').html("Akses Pelapor");
        $('#btnAkses').html("Akses Petugas");
        $('#role_id').val(1);
        $('#img-icon').attr('src', 'public/1.png');
    }
});

init();