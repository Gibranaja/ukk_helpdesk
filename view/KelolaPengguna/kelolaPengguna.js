var table;
function init(){
}

$(document).ready(function(){
    var t = table = $('#user-table').dataTable({
        "aProcessing" : true, 
        "aServerSide" : true,
        "ajax": {
            url: "../../controller/User.php?op=getAllUser",
            type: "POST",
            dataType: "json",
            data: { user_id: user_id },
            error: function (e) {
                console.log(e.responseText);
            }
        },

    }).DataTable();
    t.on( 'order.dt search.dt', function () {
        let i = 1;
 
        t.cells(null, 0, {search:'applied', order:'applied'}).every( function (cell) {
            this.data(i++);
        } );
    } ).draw();
}); 

init();