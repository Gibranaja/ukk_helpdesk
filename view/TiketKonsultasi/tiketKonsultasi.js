var table;
var user_id = $('#user_id').val();
var role_id = $('#role_id').val();

function init(){

}

$(document).ready(function(){

    if(role_id == 1){
        var t = table = $('#ticket-table').dataTable({
            "aProcessing" : true, 
            "aServerSide" : true,
            "ajax": {
                url: "../../controller/Ticket.php?op=getData",
                type: "POST",
                dataType: "json",
                data: { user_id: user_id },
                error: function (e) {
                    console.log(e.responseText);
                }
            },
            
            
        }).DataTable();
    }else{
        var t = table = $('#ticket-table').dataTable({
            "aProcessing" : true, 
            "aServerSide" : true,
            "ajax": {
                url: "../../controller/Ticket.php?op=get_data_petugas",
                type: "POST",
                dataType: "json",
                error: function (e) {
                    console.log(e.responseText);
                }
            },
           
            
        }).DataTable();
    }

    

    t.on( 'order.dt search.dt', function () {
        let i = 1;
 
        t.cells(null, 0, {search:'applied', order:'applied'}).every( function (cell) {
            this.data(i++);
        } );
    } ).draw();
    
    
    
    

    
}); 

function get(tick_id){
    console.log(tick_id);
}

init();