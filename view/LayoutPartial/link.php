    <link href="../../public/img/favicon.144x144.png" rel="apple-touch-icon" type="image/png" sizes="144x144">
	<link href="../../public/img/favicon.114x114.png" rel="apple-touch-icon" type="image/png" sizes="114x114">
	<link href="../../public/img/favicon.72x72.png" rel="apple-touch-icon" type="image/png" sizes="72x72">
	<link href="../../public/img/favicon.57x57.png" rel="apple-touch-icon" type="image/png">
	<link href="../../public/img/favicon.png" rel="icon" type="image/png">
	<link href="../../public/img/favicon.ico" rel="shortcut icon">

	
    <link rel="stylesheet" href="../../public/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="../../public/css/lib/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="../../public/css/main.css">

	<!-- summernote editor -->
	<link rel="stylesheet" href="../../public/css/lib/summernote/summernote.css">
	<link rel="stylesheet" href="../../public/css/separate/pages/editor.min.css">

	<!-- sweetalert -->
	<link rel="stylesheet" href="../../public/css/lib/bootstrap-sweetalert/sweetalert.css">
	<link rel="stylesheet" href="../../public/css/separate/vendor/sweet-alert-animations.min.css">

	<!-- datatable -->
	<link rel="stylesheet" href="../../public/css/lib/datatables-net/datatables.min.css">
	<link rel="stylesheet" href="../../public/css/separate/vendor/datatables-net.min.css">