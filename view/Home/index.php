<?php
    require_once("../../config/Connect.php");
    if(isset($_SESSION["user_id"])){

?>
<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Help Desk Application|Home</title>

    <!-- Link -->
	<?php require_once("../LayoutPartial/link.php"); ?>
    <!-- end link -->

</head>
<body class="with-side-menu">

    <!-- header -->
	<?php require_once("../LayoutPartial/header.php"); ?>
    <!-- end header -->

	<div class="mobile-menu-left-overlay"></div>

    <!-- nav -->
	<?php require_once("../LayoutPartial/nav.php"); ?>
    <!-- end nav -->

	<div class="page-content">
		<div class="container-fluid">
			Blank page.
		</div><!--.container-fluid-->
	</div><!--.page-content-->

    <!-- script -->
	<?php require_once("../LayoutPartial/script.php"); ?>
    <!-- end script -->
    <script src="home.js" type="text/javascript"></script>

</body>
</html>
<?php
    }else{
        header("Location: ".Connect::base_url()."index.php");
    }
?>