	function init(){
	$("#ticket-form").on("submit", function(e){
		saveAndEdit(e);
	})
}

		$(document).ready(function() {
			$('#text-editor').summernote({
                height: 150
            })

			$.post("../../controller/Category.php?op=combo", function(data, status){
				$('#cat_select').html(data); 
			})

		
		});

		function saveAndEdit(e){
			e.preventDefault();
			var formData = new FormData($("#ticket-form")[0]);

			// Validate the form  before insert

			if ($('#text-editor').val() == '' || $('#tick_name').val() == ''){
				swal("Perhatian", "Silahkan Isi Form Terlebih Dahulu", "warning");
			} else {
			$.ajax({
				url: "../../controller/Ticket.php?op=insert",
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
					$('#tick_name').val('');
					$('#text-editor').summernote('reset');
					swal("Selamat", "Tiket Baru Disimpan", "success");
				}
			})
		}
		}

		init();