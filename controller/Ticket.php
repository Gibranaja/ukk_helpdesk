<?php
    require_once("../config/Connect.php");
    require_once("../models/Ticket.php");


    $ticket = new Ticket();

    switch($_GET["op"]){
        case "insert" :
            $ticket->insert_ticket($_POST["user_id"], $_POST["cat_id"], $_POST["tick_name"], $_POST["tick_descrip"]);
        break;

        case "getData" :
            $data_hasil = $ticket->get_data($_POST["user_id"]);
            $data = array();
            foreach($data_hasil as $row){
                $sub_array = array();
                $sub_array[] = $row["tick_id"];
                $sub_array[] = $row["cat_name"];
                $sub_array[] = $row["tick_name"];

                if($row["tick_status"] == "Opened"){
                    $sub_array[] = '<span class="label label-success">Opened</span>';
                }else{
                    $sub_array[] = '<span class="label label-danger">Closed</span>';
                }

                $sub_array[] = date("d/M/Y", strtotime($row["created_at"]));
                $sub_array[] = '<button type="button" onClick="get('.$row["tick_id"].');" id="'.$row["tick_id"].'"
                class="btn btn-rounded btn-inline btn-success-outline btn-sm" title="Edit Ticket">
                    <div>
                        <i class="fa fa-edit"></i>
                    </div>
                </button>';
                $data[] = $sub_array;
            }

            $result = array(
                "sEcho" => 1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );

            echo json_encode($result);
        break;
        case "get_data_petugas" :
            $data_hasil = $ticket->get_data_petugas();
            $data = array();
            foreach($data_hasil as $row){
                $sub_array = array();
                $sub_array[] = $row["tick_id"];
                $sub_array[] = $row["cat_name"];
                $sub_array[] = $row["tick_name"];

                if($row["tick_status"] == "Opened"){
                    $sub_array[] = '<span class="label label-success">Opened</span>';
                }else{
                    $sub_array[] = '<span class="label label-danger">Closed</span>';
                }

                $sub_array[] = date("d/M/Y", strtotime($row["created_at"]));
                $sub_array[] = '<button type="button" onClick="get('.$row["tick_id"].');" id="'.$row["tick_id"].'"
                class="btn btn-rounded btn-inline btn-success-outline btn-sm" title="Edit Ticket">
                    <div>
                        <i class="fa fa-edit"></i>
                    </div>
                </button>';
                $data[] = $sub_array;
            }

            $result = array(
                "sEcho" => 1,
                "iTotalRecords" => count($data),
                "iTotalDisplayRecords" => count($data),
                "aaData" => $data
            );

            echo json_encode($result);
        break;
    }