<?php
    class Ticket extends Connect{
        public function insert_ticket($user_id, $cat_id, $tick_name, $tick_descrip){

            $connect = parent::connection();
            parent::set_name();

            $sql = "INSERT INTO tb_tiket (tick_id, user_id, cat_id, tick_name, tick_descrip, tick_status, created_at, status) VALUES(NULL,?,?,?,?,'Opened',now(),'1')";

            $sql = $connect->prepare($sql);

            $sql->bindValue(1, $user_id);
            $sql->bindValue(2, $cat_id);
            $sql->bindValue(3, $tick_name);
            $sql->bindValue(4, $tick_descrip);

            $sql->execute();

            return $result=$sql->fetchAll();
        }

        public function get_data($user_id){

            $connect =  parent::connection();
            parent::set_name();

            // sql join table user, ticket, category
            $sql = "SELECT 
                    tb_tiket.tick_id, 
                    tb_tiket.user_id, 
                    tb_tiket.cat_id, 
                    tb_tiket.tick_name, 
                    tb_tiket.tick_descrip,
                    tb_tiket.tick_status,
                    tb_tiket.created_at, 
                    tb_user.user_fullname, 
                    tb_category.cat_name 
                    FROM 
                    tb_tiket 
                    INNER JOIN tb_category ON tb_tiket.cat_id = tb_category.cat_id
                    INNER JOIN tb_user ON tb_tiket.user_id = tb_user.user_id
                    WHERE 
                    tb_tiket.status = 1
                    AND
                    tb_user.user_id=?";
            
            $sql = $connect->prepare($sql);
            $sql->bindValue(1 , $user_id);
            $sql->execute();
            return $result = $sql->fetchAll();


        }

        public function get_data_petugas(){
            $connect =  parent::connection();
            parent::set_name();

            // sql join table user, ticket, category
            $sql = "SELECT 
                    tb_tiket.tick_id, 
                    tb_tiket.user_id, 
                    tb_tiket.cat_id, 
                    tb_tiket.tick_name, 
                    tb_tiket.tick_descrip,
                    tb_tiket.tick_status,
                    tb_tiket.created_at, 
                    tb_user.user_fullname, 
                    tb_category.cat_name 
                    FROM 
                    tb_tiket 
                    INNER JOIN tb_category ON tb_tiket.cat_id = tb_category.cat_id
                    INNER JOIN tb_user ON tb_tiket.user_id = tb_user.user_id
                    WHERE 
                    tb_tiket.status = 1";
            
            $sql = $connect->prepare($sql);
            $sql->execute();
            return $result = $sql->fetchAll();

        }
    }